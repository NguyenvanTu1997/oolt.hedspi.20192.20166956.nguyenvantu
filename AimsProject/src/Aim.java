
public class Aim {
	public static void main(String[] args) {
		Order demo = new Order();
		
		demo.addDigitalVideoDisc(new DigitalVideoDisc("Lion King", "Animation", "Roger Allers", 87, 19.95f));
		demo.addDigitalVideoDisc(new DigitalVideoDisc("Star World", "Science Fiction", "George Lucas", 124, 24.95f));
		demo.addDigitalVideoDisc(new DigitalVideoDisc("Aladdin", "Animation", "John Musker", 90, 18.99f));
		
		System.out.println("Total cost: " + demo.totalCost());
		
	}

}
