
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class MyDate {
	private int date, month, year;

	public MyDate() {
		Date today = new Date();
		date = Integer.parseInt(new SimpleDateFormat("dd").format(today));
		month = Integer.parseInt(new SimpleDateFormat("mm").format(today));
		year = Integer.parseInt(new SimpleDateFormat("yyyy").format(today));
	}

	public MyDate(int date, int month, int year) {
		super();
		this.date = date;
		this.month = month;
		this.year = year;
		
		if (!validate()) System.out.println(this.date + "-" + this.month + "-" + this.year + " not valid");
		
		while (!validate()) {
			this.date--;
		}
		
	}

	public MyDate(String input) {
		// e.g. "Jul-12-1999"
		SimpleDateFormat parser = new SimpleDateFormat("MMM-dd-yyyy");

		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd");
		SimpleDateFormat monthFormatter = new SimpleDateFormat("MM");
		SimpleDateFormat yearFormatter = new SimpleDateFormat("yyyy");

		try {
			this.date = Integer.parseInt(dateFormatter.format(parser.parse(input)));
			this.month = Integer.parseInt(monthFormatter.format(parser.parse(input)));
			this.year = Integer.parseInt(yearFormatter.format(parser.parse(input)));
		} catch (NumberFormatException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getDate() {
		return date;
	}

	public void setDate(int date) {
		if (date > 0 && date < 32) {
			this.date = date;
			return;
		}
		System.out.println("Date not valid!");
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		if (month > 0 && month < 13) {
			this.month = month;
			return;
		}
		System.out.println("Month not valid!");
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public boolean validate() {
		if (date == 31 && !(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12))
			return false;
		
		if (date==30 && month==2)
			return false;
		
		if (date==29 && month==2 && !isLeapYear())
			return false;
		
		return true;
	}

	public boolean isLeapYear() {
		if (year % 4 == 0) {
			if (year % 100 == 0) {
				if (year % 400 == 0)
					return true;
				else
					return false;
			} else
				return true;
		} else
			return false;
	}

	public void print() {
		System.out.println(this.date + "-" + this.month + "-" + this.year);
	}

	public void accept() {
		Scanner keyboard = new Scanner(System.in);
		System.out.print("Enter a date(e.g. \"Jul-12-1999\") : ");
		String input = keyboard.nextLine();

		SimpleDateFormat parser = new SimpleDateFormat("MMM-dd-yyyy");

		SimpleDateFormat dateFormatter = new SimpleDateFormat("dd");
		SimpleDateFormat monthFormatter = new SimpleDateFormat("MM");
		SimpleDateFormat yearFormatter = new SimpleDateFormat("yyyy");

		try {
			this.date = Integer.parseInt(dateFormatter.format(parser.parse(input)));
			this.month = Integer.parseInt(monthFormatter.format(parser.parse(input)));
			this.year = Integer.parseInt(yearFormatter.format(parser.parse(input)));
		} catch (NumberFormatException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		keyboard.close();

	}

}