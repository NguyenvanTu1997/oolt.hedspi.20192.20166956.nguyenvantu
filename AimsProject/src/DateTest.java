
public class DateTest {

	public static void main(String[] args) {
		MyDate demo1 = new MyDate();
		MyDate demo2 = new MyDate(30, 2, 2019);
		MyDate demo3 = new MyDate("Jul-12-1999");

		demo1.print();
		demo2.print();
		demo3.print();
		
		MyDate demo4 = new MyDate();
		demo4.accept();
		demo4.print();

	}

}
