
public class Order {
	public static final int MAX_NUMBERS_ORDERED = 3;
	
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	
	private int qtyOrdered = 0;

	public int getQtyOrdered() {
		return qtyOrdered;
	}

	public void setQtyOrdered(int qtyOrdered) {
		this.qtyOrdered = qtyOrdered;
	}
	
	public void getItemsOrdered() {
		for (int i = 0; i < itemsOrdered.length; i++) {
			System.out.println(itemsOrdered[i]);
		}
	}
	
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		
		if (this.qtyOrdered < MAX_NUMBERS_ORDERED) {
			this.itemsOrdered[this.qtyOrdered++] = disc;
			System.out.println("The disc \"" + disc.getTitle() + "\" has been added");
			return;
		}
		System.out.println("The order is almost full");
	}
	
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		for (int i = 0; i < this.qtyOrdered; i++) {
			if (disc.getTitle().equals(this.itemsOrdered[i].getTitle())) {
				this.qtyOrdered--;
				for (int j = i; j < this.qtyOrdered ; j++) {
					this.itemsOrdered[j] = this.itemsOrdered[j+1];
				}
			}
		}
		
	}
	
	public float totalCost() {
		float total = 0;
		
		for (int i = 0; i < itemsOrdered.length; i++) {
			total += itemsOrdered[i].getCost();
		}
		
		return total;
	}

}
